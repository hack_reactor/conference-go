import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture_url(city, state):
    url = "https://api.pexels.com/v1/search"
    url_params = {
        "query": f"{city},&20{state}",
    }
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    r = requests.get(
        url=url,
        headers=headers,
        params=url_params,
    )
    content = json.loads(r.content)
    picture = {
        "picture_url": content["photos"][0]["url"],
    }
    return picture.get("picture_url")


def get_location(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct?country=US"
    params = {
        "q": f"{city},&20{state}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url=url, params=params)
    content = json.loads(r.content)
    location = {
        "latitude": content[0]["lat"],
        "longitude": content[0]["lon"],
    }
    return location.get("latitude"), location.get("longitude")


def get_weather(lat, lon):
    url = "https://api.openweathermap.org/data/2.5/weather?units=imperial"
    url_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url=url, params=url_params)
    content = json.loads(r.content)
    if content != {}:
        weather = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
        return weather
    else:
        return None
